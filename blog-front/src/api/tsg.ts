import {IResponseType, pageDataInfo} from "@/types/shims-axios";
import http from "@/utils/http";

/**
 * 获取所有标签的文章数量
 * @returns promise
 */
export const getTagCountListApi = () => {
    return http.get<IResponseType<ITag[]>>("/blog-api/tag/tagCountList")
}
/**
 * 获取所有标签
 * @returns promise
 */
export const getTagListApi = () => {
    return http.get<IResponseType<ITag[]>>("/blog-api/tag/list")
}