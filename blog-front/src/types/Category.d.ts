interface ICategory {
    id: number
    count: number
    name: string
    pid: number
}

interface ICategoryVo {
    id: number
    description: string
    name: string
    pid: number
}