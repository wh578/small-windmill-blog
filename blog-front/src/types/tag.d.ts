interface ITag {
    count: number,
    id: number,
    name: string

}

interface ITagVo {
    id: number,
    name: string

}