import {getTagCountListApi, getTagListApi} from "@/api/tsg";
import {defineStore} from "pinia";
import {ElMessage} from "element-plus";


export interface TagAboutState {
    tagCounts?: ITag[],
    tags?: ITag[]
}

export const useTagAboutStore = defineStore("tagAbout", {
    state: (): TagAboutState => ({
        tagCounts: [],
        tags: []
    }),
    getters: {
        /**
         * 获取标签
         * @returns 标签集合
         */
        getTagCounts(): ITag[] | undefined {
            return this.tagCounts
        },
        /**
         * 获取all标签
         * @returns 标签所有集合
         */
        getTags(): ITag[] | undefined {
            return this.tags
        }
    },
    actions: {
        setTagCounts(tagCounts: ITag[]): void {
            this.tagCounts = tagCounts
        },
        setTags(tags: ITag[]): void {
            this.tags = tags
        },
        async getTagCountsApi() {
            await getTagCountListApi().then(res => {
                if (res.code === 200) {
                    this.setTagCounts(res.data)
                }
            }).catch(err => {
                ElMessage.error("服务器错误！！")
            })
        },
        async getTagsApi() {
            await getTagListApi().then(res => {
                if (res.code === 200) {
                    this.setTags(res.data)
                }
            }).catch(err => {
                ElMessage.error("服务器错误！！")
            })
        }
    }

})