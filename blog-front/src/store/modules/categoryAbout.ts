import {getCategoryCountList, getCategoryList} from "@/api/category";
import {defineStore} from "pinia";

export interface CategoryAboutState {
    categoryCounts?: ICategory[],
    categoryList?: ICategoryVo[]
}

export const useCategoryAboutStore = defineStore("categoryCounts", {
    state: (): CategoryAboutState => {
        return {
            categoryCounts: [],
            categoryList: []
        }
    },
    getters: {
        /**
         * 获取标签
         * @returns 标签集合
         */
        getCategoryCountList(): ICategory[] | undefined {
            return this.categoryCounts
        },
        /**
         * 获取all标签
         * @returns 标签所有集合
         */
        getCategoryList(): ICategoryVo[] | undefined {
            return this.categoryList
        }
    },
    actions: {
        setCategory(categoryCounts: ICategory[]): void {
            this.categoryCounts = categoryCounts
        },
        setCategoryList(categoryList: ICategoryVo[]): void {
            this.categoryList = categoryList
        },
        async getCategoryCountsApi() {
            await getCategoryCountList().then((res) => {
                if (res.data) {
                    this.setCategory(res.data)
                }
            })
        },
        async getCategoryListApi() {
            await getCategoryList().then((res) => {
                if (res.data) {
                    this.setCategoryList(res.data)
                }
            })
        },
    }
})