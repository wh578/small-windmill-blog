import {defineConfig} from 'vite'
import vue from "@vitejs/plugin-vue";
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'

// https://vitejs.dev/config/
export default defineConfig(({command, mode}) => {
    return {
        base: mode === 'v3production' ? '/blog-front/' : './',// 设置打包路径
        plugins: [
            vue(),
            AutoImport({
                resolvers: [ElementPlusResolver()],
            }),
            Components({
                resolvers: [ElementPlusResolver()],
            }),
        ],
        resolve: {
            alias: {
                "@": "/src",
            },
        },
        server: {
            port: 18084, // 设置服务启动端口号
            host: "0.0.0.0",
            proxy: {
                "/blog-api": {
                    target: "http://127.0.0.1:9090",
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/blog-api/, ""),
                },
            },
        },
    }
})

